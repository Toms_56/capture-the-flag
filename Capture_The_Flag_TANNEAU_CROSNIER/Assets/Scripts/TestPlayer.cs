using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TestPlayer : MonoBehaviour
{
    // Essayer de faire en sorte de tout mettre sur objet creation
    // En gros faire le ontriggerenter sur l'objet avec le tag player
    // Du coup a la colision faire en sorte de remplir le premier ou le deuxi�me slot
    // Penser � comment aurai march� l'�change d'objet

    public Inventory inventory;
    public float speed;
    public int index = 0;
    public int pv = 10;
    public Text textSwap;
    public bool canSwap = false;

    public string colorTeam;
    public string colorEnemy;

    public bool indestructible = false;
    public bool haveFlag= false;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
         if (Input.GetKeyDown("space") && inventory.slots[index].currentItem)
         {
             inventory.slots[index].currentItem.Use();
         }

         if (Input.GetKeyDown(KeyCode.Z))
         {
            index++;

            if (index > inventory.slots.Length - 1)
            {
                index = 0;
            }

            for (int i = 0; i < inventory.slots.Length; i++)
            {
                Debug.Log("I : " + i);
                inventory.slots[i].isEquip = false;
            }
         }
        
        inventory.slots[index].isEquip = true;


        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(new Vector3(speed, 0, 0));

        }
        if (Input.GetKey(KeyCode.LeftArrow)){
            transform.Translate(new Vector3(-speed, 0, 0));
        }

        if (pv == 0)
        {
           /* if (inventory.slots[0].currentItem.gameObject.GetComponent<Flag>())
            {
                inventory.slots[0].currentItem.gameObject.transform.position = transform.position;
                inventory.slots[0].currentItem.gameObject.SetActive(true);
                inventory.slots[0].currentItem.gameObject.tag = "Flag";
                Destroy(gameObject);

            }
            else if (inventory.slots[1].currentItem.gameObject.GetComponent<Flag>())
            {
                inventory.slots[1].currentItem.gameObject.transform.position = transform.position;
                inventory.slots[1].currentItem.gameObject.SetActive(true);
                Destroy(gameObject);
            }*/

            inventory.slots[0].currentItem = null;
            inventory.slots[1].currentItem = null;
            Destroy(gameObject);
        }
        TxtSwap();
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Flag")
        {
            /*if (!inventory.slots[0].currentItem)
            {
                inventory.AddFlag(inventory.slots[0], other.GetComponent<ObjectCreation>());
                other.tag = "Untagged";
                other.gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
            else if (!inventory.slots[1].currentItem)
            {
                inventory.AddFlag(inventory.slots[1], other.GetComponent<ObjectCreation>());
                other.tag = "Untagged";
                other.gameObject.GetComponent<MeshRenderer>().enabled = false;
            }*/

            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if (!inventory.slots[i].currentItem)
                {
                    inventory.slots[i].AddObject(other.GetComponent<ObjectCreation>());
                    //inventory.AddFlag(inventory.slots[i], other.GetComponent<ObjectCreation>());
                    other.tag = "Untagged";
                    inventory.slots[i].flagEquip = true;
                    other.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    break;
                }
            }
        }

        Spawner spawner = other.GetComponent<Spawner>();
        if (spawner == null) return;
        

        if (spawner && other.tag != "Food"  && other.tag != "Untagged")
        {
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if (!inventory.slots[i].currentItem)
                {
                    inventory.AddObject(spawner, inventory.slots[i]);
                    SpawnerManager.instance.listObData.Remove(spawner.createObject.dataForList);
                    other.tag ="Untagged";
                    break;
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        Spawner spawner = other.GetComponent<Spawner>();
        if (inventory.slots[index].currentItem && canSwap /*other.tag != "Untagged"*/)
        {
            if (Input.GetKey(KeyCode.A))
            {
                if (!inventory.slots[index].flagEquip)
                {
                    if (other.tag == "Flag")
                    {
                        SpawnerManager.instance.listObData.Add(inventory.slots[index].currentItem.dataForList);
                        Destroy(inventory.slots[index].currentItem.gameObject);
                        inventory.slots[index].AddObject(other.GetComponent<ObjectCreation>());
                        //inventory.AddFlag(inventory.slots[index], other.GetComponent<ObjectCreation>());
                        other.tag = "Untagged";
                        other.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    }
                    else if (other.tag == "Zone" + colorEnemy)
                    {
                        ZoneFlag zoneFlag = other.GetComponent<ZoneFlag>();
                        SpawnerManager.instance.listObData.Add(inventory.slots[index].currentItem.dataForList);
                        Destroy(inventory.slots[index].currentItem.gameObject);
                        inventory.slots[index].AddObject(zoneFlag.flag);
                        inventory.slots[index].flagEquip = true;
                        zoneFlag.flagMesh.enabled = false;
                    }
                    else if (spawner && other.tag != "Untagged")
                    {
                        Debug.Log("Casse couille2");
                        inventory.slots[index].currentItem.SwappingItem(spawner);
                    }

                    /*Debug.Log("Try stupid");
                    SpawnerManager.instance.listObData.Add(inventory.slots[index].currentItem.dataForList);
                    Destroy(inventory.slots[index].currentItem.gameObject);

                    //inventory.slots[index].currentItem.Destruct();

                    Debug.Log("Exchange");
                    inventory.AddObject(spawner, inventory.slots[index]);
                    SpawnerManager.instance.listObData.Remove(spawner.createObject.dataForList);
                    spawner.objectSave = true;
                    canSwap = false;
                    other.tag = "Untagged";*/
                }
                /*if (other.tag == "Flag")
                {
                    SpawnerManager.instance.listObData.Add(inventory.slots[index].currentItem.dataForList);
                    Destroy(inventory.slots[index].currentItem.gameObject);

                    inventory.AddFlag(inventory.slots[index],other.GetComponent<ObjectCreation>());
                    other.tag = "Untagged";
                    other.gameObject.GetComponent<MeshRenderer>().enabled = false;
                }*/
            }

            /*if (inventory.slots[index].flagEquip == false)
            {
                if (Input.GetKey(KeyCode.A))
                {
                    Debug.Log("Try stupid");
                    SpawnerManager.instance.listObData.Add(inventory.slots[index].currentItem.dataForList);
                    Destroy(inventory.slots[index].currentItem.gameObject);

                    //inventory.slots[index].currentItem.Destruct();

                    Debug.Log("Exchange");
                    //inventory.AddObject(spawner, inventory.slots[index]);
                    SpawnerManager.instance.listObData.Remove(spawner.createObject.dataForList);
                    spawner.objectSave = true;
                    canSwap = false;
                    other.tag = "Untagged";
                }
            }
            if (inventory.slots[index].flagEquip)
            {
                if (Input.GetKey(KeyCode.A))
                {
                    Debug.Log("Detection flag");
                    SpawnerManager.instance.listObData.Add(inventory.slots[index].currentItem.dataForList);
                    Destroy(inventory.slots[index].currentItem.gameObject);

                    inventory.AddFlag(inventory.slots[index], other.GetComponent<ObjectCreation>());
                    other.tag = "Untagged";
                    other.gameObject.GetComponent<MeshRenderer>().enabled = false;
                }
            }*/
        }
    }

    private void OnTriggerExit(Collider other)
    {
        canSwap = false;
    }


    private void TxtSwap()
    {
        if (canSwap)
        {
            textSwap.enabled = true;
            textSwap.text = "Press " + KeyCode.A + " for swap item";
        }
        else
        {
            textSwap.enabled = false;
        }
    }
}
