using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if (player.indestructible)
            {
                player.indestructible = false;
                for (int i = 0; i < player.inventory.slots.Length; i++)
                {
                    if (player.inventory.slots[i].currentItem.GetComponent<Chestplate>())
                    {
                        player.inventory.slots[i].currentItem.Destruct();
                    }
                }
            }
            else
            {
                player.pv -= 1;
                Debug.Log("Player Pv : " + player.pv);
            }
        }
    }
}
