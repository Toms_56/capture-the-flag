using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunCanon : MonoBehaviour
{
    public GameObject bullet;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void Shoot()
    {
        Instantiate(bullet.gameObject, transform.position, transform.rotation);
    }
}
