using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ZoneFlag : MonoBehaviour
{
    public string color;
    public Transform saveFlagPosition;
    public ObjectCreation flag;
    public MeshRenderer flagMesh;

    public GameObject panelWin;

    private bool isPaused;

    //public GameObject panelFlagToken;
    
    // Start is called before the first frame update
    void Start()
    {
        isPaused = false;
        Time.timeScale = 1;
        //flag = GetComponentInChildren<ObjectCreation>();
        //flagMesh = flag.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(isPaused)
        {
            Time.timeScale = 0;
            if (Input.GetKeyDown(KeyCode.V))
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            // If is an enemy, he take the flag;
            if (player.colorTeam != color)
            {
                for (int i = 0; i < player.inventory.slots.Length; i++)
                {
                    if (!player.inventory.slots[i].currentItem && flagMesh.enabled)
                    {
                        player.inventory.slots[i].AddObject(flag);
                        player.inventory.slots[i].flagEquip = true;
                        flagMesh.enabled = false;
                        break;
                    }
                    else
                    {
                        flag.GetComponent<Flag>().Swapping(other);
                    }
                }
            }

            if (player.colorTeam == color)
            {
                for (int i = 0; i < player.inventory.slots.Length; i++)
                {
                    if (player.inventory.slots[i].flagEquip)
                    {
                        Flag flag = player.inventory.slots[i].currentItem.GetComponent<Flag>();
                        // Replace the flag
                        if (flag.color == color)
                        {
                            flag.player = null;
                            player.inventory.slots[i].flagEquip = false;
                            player.inventory.slots[player.index].currentItem = null;
                            flag.transform.position = saveFlagPosition.position;
                            flagMesh.enabled = true;
                            //panelFlagToken.SetActive(true);
                            break;
                        }
                        else
                        {
                            Debug.Log("Victory");
                            panelWin.SetActive(true);
                            isPaused = true;
                            
                        }
                    }
                }
            }
        }
    }
}
