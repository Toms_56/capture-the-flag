using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    public ObjectCreation currentItem;
    public Image equipIcon;
    public Image icon;
    public bool isEquip;
    public bool flagEquip;
    Player player;

    // Mettre du text pour le nombre d'utilisation du Grapplink
    void Start()
    {
        player = GetComponentInParent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentItem)
        {
            icon.sprite = currentItem.icon;
            icon.enabled = true;
            //slotEmpty = true;

            if (currentItem.GetComponent<GrapplingHook>())
            {
                // Text nbrUtil
            }

            // Features for know Chestplate is activate
            if (player.indestructible && currentItem.GetComponent<Chestplate>())
            {
                icon.color = Color.blue;
            }
        }
        else
        {
            icon.sprite = null;
            icon.enabled = true;
            //slotEmpty = false;
        }

        //Features for know actif item
        if (isEquip)
        {
            equipIcon.enabled = true;
        }
        else
        {
            equipIcon.enabled = false;
        }
    }

    // Fonction for add object to the slot
    public void AddObject (ObjectCreation objectCreation)
    {
        currentItem = objectCreation;
        currentItem.player = GetComponentInParent<Player>();
        //currentItem.isEquip = true;
    }
}
