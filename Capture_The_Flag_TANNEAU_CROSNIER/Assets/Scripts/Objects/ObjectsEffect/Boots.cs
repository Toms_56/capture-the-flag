using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boots : ObjectCreation
{
    bool canUse = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void Update()
    {
        base.Update();
    }


    public override void Use()
    {
        if (canUse)
        {
            StartCoroutine(Upspeed());
            canUse = false;
        }
    }

    IEnumerator Upspeed()
    {
        player.speed = player.speed * 1.5f;
        yield return new WaitForSeconds(10);
        player.speed = player.speed / 1.5f;
        Destruct();
    }
}
