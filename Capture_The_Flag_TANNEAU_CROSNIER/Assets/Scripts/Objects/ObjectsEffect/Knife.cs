using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : ObjectCreation
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void Update()
    {
        base.Update();
        if (findPlayer)
        {
            player.seeObject[1].SetActive(true);
        }
    }

    public override void Use()
    {
        StartCoroutine(Knifecut());
    }


    IEnumerator Knifecut()
    {
        player.anim.SetBool("isStabbing", true);
        yield return new WaitForSeconds(2.5f);
        player.seeObject[1].SetActive(false);
        player.anim.SetBool("isStabbing", false);
        Destruct();
    }
}
