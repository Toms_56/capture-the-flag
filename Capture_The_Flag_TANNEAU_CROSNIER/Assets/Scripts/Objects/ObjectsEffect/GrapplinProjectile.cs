using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplinProjectile : MonoBehaviour
{
    public GameObject target;
    private Vector3 startPos;

    //public float speed;

    private void Start()
    {
        startPos = gameObject.transform.position;
    }


    public void Shoot()
    {
        gameObject.transform.position = target.transform.position;
        StartCoroutine(ReloadGrapplin());
        //transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
        //transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    IEnumerator ReloadGrapplin()
    {
        yield return new WaitForSeconds(1);
        gameObject.transform.position = startPos;
    }
}
