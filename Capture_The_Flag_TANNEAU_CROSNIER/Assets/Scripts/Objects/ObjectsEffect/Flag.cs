using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : ObjectCreation
{
    public string color;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if(findPlayer)
        {
            if(player.pv == 0)
            {
                Destruct();
            }
        }
    }
    public override void Use()
    {

    }

    public override void Destruct()
    {
        gameObject.transform.position = player.transform.position;
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        gameObject.tag = "Flag";
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && tag != "Untagged")
        {
            Swapping(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            player.canSwap = false;
        }
    }

    public void Swapping(Collider other)
    {
        Player player = other.GetComponent<Player>();

        int checkEmptySlot = 0;

        for (int i = 0; i < player.inventory.slots.Length; i++)
        {
            if (player.inventory.slots[i].currentItem)
            {
                checkEmptySlot += 1;
            }
        }

        if (player.inventory.slots[player.index].currentItem && player.inventory.slots[player.index].currentItem.GetComponent<Flag>())
        {
            checkEmptySlot -= 1;
        }

        //Debug.Log("EmptySlot" + checkEmptySlot);

        if (checkEmptySlot == player.inventory.slots.Length)
        {
            player.canSwap = true;
        }
    }
}
