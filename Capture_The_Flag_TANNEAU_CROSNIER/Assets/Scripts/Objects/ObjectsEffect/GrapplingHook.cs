using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingHook : ObjectCreation
{
    
    // Start is called before the first frame update
    void Start()
    {
        nbrUtil = 3;
    }
    
    public override void Update()
    {
        base.Update();
        if (findPlayer)
        {
            player.seeObject[2].SetActive(true);
        }
    }

    public override void Use()
    {
        player.GetComponentInChildren<GrapplinProjectile>().Shoot();
        base.Use();
    }
    
    public override void Destruct()
    {
        base.Destruct();
        player.seeObject[2].SetActive(false);
    }
}
