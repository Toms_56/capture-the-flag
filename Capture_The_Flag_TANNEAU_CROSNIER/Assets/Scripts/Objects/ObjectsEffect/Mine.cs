using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : ObjectCreation
{
    MeshRenderer mesh;
    bool canExplose = false;
    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshRenderer>();
    }

    public override void Update()
    {
        base.Update();

        if(nbrUtil == 0)
        {
            Destruct();
        }
    }


    public override void Use()
    {
        transform.position = player.transform.position;
        player.inventory.slots[player.index].currentItem = null;

        StartCoroutine(Explosion());
        
        Debug.Log("Use Mine");
    }

    IEnumerator Explosion()
    {
        mesh.enabled = true;
        yield return new WaitForSeconds(1.5f);
        canExplose = true;
        mesh.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag =="Player" && canExplose)
        {
            player.pv = 0;
            nbrUtil--;
        }
    }
}
