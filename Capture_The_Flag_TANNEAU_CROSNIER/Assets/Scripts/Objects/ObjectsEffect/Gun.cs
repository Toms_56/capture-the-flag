using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Gun : ObjectCreation
{
    // Start is called before the first frame update
    void Start()
    {
        nbrUtil = 3;
    }

    void Update()
    {
        base.Update();
        if (findPlayer)
        {
            player.seeObject[0].SetActive(true);
        }
    }
    
    public override void Use()
    {
        player.GetComponentInChildren<GunCanon>().Shoot();
        base.Use();
    }

    public override void Destruct()
    {
        base.Destruct();
        player.seeObject[0].SetActive(false);
    }
}
