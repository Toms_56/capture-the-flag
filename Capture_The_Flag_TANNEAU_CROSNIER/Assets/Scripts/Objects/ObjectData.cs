using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Object", menuName = "Create Object")]
public class ObjectData : ScriptableObject
{
    public enum type
    {
        Boots,
        Chestplate,
        Food,
        GrapplingHook,
        Gun,
        Knife,
        Mine,
        Flag,
    }

    public type objectType;
    public string objectName;
    public int nbrUtil;
    public GameObject objectPrefab;
    public Sprite icon;
}
