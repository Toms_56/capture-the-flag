using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCreation : MonoBehaviour
{
    public ObjectData.type objectType;
    public ObjectData dataForList;
    public string objectName;
    public int nbrUtil;
    public GameObject objectPrefab;
    public Sprite icon;

    public Player player = null;
    public bool findPlayer;

    void Start()
    {
        
    }

    // Update is called once per frame
    public virtual void Update()
    {
        if (player)
        {
            findPlayer = true;
        }
        else
        {
            findPlayer = false;
        }
        
    }

    // Associate to the data
    public virtual void Creation(ObjectData objectData)
    {
        objectType = objectData.objectType;
        objectName = objectData.objectName;
        nbrUtil = objectData.nbrUtil;
        objectPrefab = objectData.objectPrefab;
        icon = objectData.icon;
        dataForList = objectData;
    }

    public virtual void Destruct()
    {
        Destroy(gameObject);
        SpawnerManager.instance.listObData.Add(dataForList);
    }

    public virtual void Use()
    {
        nbrUtil--;
        if (nbrUtil == 0)
        {
            Destruct();
            icon = null;
        }
    }

    // Fonction for swap object with other object
    public void SwappingItem(Spawner spawner)
    {
        SpawnerManager.instance.listObData.Add(dataForList);
        Destroy(player.inventory.slots[player.index].currentItem.gameObject);
        player.inventory.AddObject(spawner, player.inventory.slots[player.index]);
        SpawnerManager.instance.listObData.Remove(spawner.createObject.dataForList);
        spawner.objectSave = true;
        player.canSwap = false;
        tag = "Untagged";
    }
}
