using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // Demander s'il faut que les objets soit différents toutes les 30s
    // Remettre la bool ObjectSave a false quand il y'a un nouvel Objet

    public float timer;
    public int nbrSpawner;
    public ObjectCreation createObject;
    //bool canSpawn = false;
    public SpawnerManager spawnerManager;
    public int resetTimer;
    public bool objectSave = false;


    // Start is called before the first frame update
    void Start()
    {
        //SpawnerManager.instance.RandomNumber();
        if (spawnerManager.listObData.Count > 0)
        {
            if (nbrSpawner == 0)
            {
                Spawning(spawnerManager.randomNbr1);
            }
            else
            {
                Spawning(spawnerManager.randomNbr2);
            }
            objectSave = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;

        if (objectSave && createObject != null)
        {
            //createObject.gameObject.SetActive(false);
            createObject.gameObject.GetComponent<MeshRenderer>().enabled = false;
        }

        if (timer >= resetTimer) 
        {
            timer = 0;
            if (spawnerManager.listObData.Count > 0)
            {
                if (nbrSpawner == 0)
                {
                    Spawning(spawnerManager.randomNbr1);
                }
                else
                {
                    Spawning(spawnerManager.randomNbr2);
                }
            }
            objectSave = false;
        }
    }

    void Spawning(int randomNbr)
    {
        //Debug.Log("Nbr List : " + listObData.Count);
        if (createObject == null || objectSave)
        {
            createObject = Instantiate(spawnerManager.listObData[randomNbr].objectPrefab, transform.position, transform.rotation,transform).GetComponent<ObjectCreation>();
            SwitchType(randomNbr);
            createObject.Creation(spawnerManager.listObData[randomNbr]);
        }
        else
        {
            Destroy(createObject.gameObject);
            //Récupérer danns le parent de l'inventaire qui sera le joueur le composant Player si jamais on doit faire un lien avec des variables
            createObject = Instantiate(spawnerManager.listObData[randomNbr].objectPrefab, transform.position, transform.rotation, transform).GetComponent<ObjectCreation>();
            SwitchType(randomNbr);
            createObject.Creation(spawnerManager.listObData[randomNbr]);
        }
    }

    void SwitchType(int randomNbr)
    {
        // Penser à ramasser
        switch (spawnerManager.listObData[randomNbr].objectType)
        {
            case ObjectData.type.Boots:
                gameObject.tag = "Boots";
                break;
            case ObjectData.type.Chestplate:
                gameObject.tag = "Chestplate";
                break;
            case ObjectData.type.Food:
                gameObject.tag = "Food";
                break;
            case ObjectData.type.GrapplingHook:
                gameObject.tag = "GrapplingHook";
                break;
            case ObjectData.type.Gun:;
                gameObject.tag = "Gun";
                break;
            case ObjectData.type.Knife:
                gameObject.tag = "Knife";
                break;
            case ObjectData.type.Mine:
                gameObject.tag = "Mine";
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
       if (other.tag == "Player")
        {
            // Penser à rechanger en Player
            Player player = other.gameObject.GetComponent<Player>();
            Slot[] slots = player.inventory.slots;
            
            if (!slots[0].currentItem || !slots[1].currentItem && gameObject.tag != "Food")
            {
                objectSave = true;
            }
            else if (tag != "Untagged" && tag != "Food" && !player.inventory.slots[player.index].currentItem.GetComponent<Flag>())
            {
                player.canSwap = true;
            }
            if (gameObject.tag == "Food")
            {
                Destroy(createObject.gameObject);
                gameObject.tag = "Untagged";

                Debug.Log("TestPlayer Food ");
                player.pv ++;
                if (player.pv > 10)
                {
                    player.pv = 10;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Penser à rechanger en Player
        Player player = other.gameObject.GetComponent<Player>();

        if (gameObject.tag != "Untagged" && other.tag == "Player" && !createObject.isActiveAndEnabled)
        {
            player.canSwap = false;
            gameObject.tag = "Untagged";
        }
    }
}
