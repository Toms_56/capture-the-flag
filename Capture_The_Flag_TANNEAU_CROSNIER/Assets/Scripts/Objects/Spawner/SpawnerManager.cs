using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SpawnerManager : MonoBehaviour
{
    public List<ObjectData> listObData;
    public static SpawnerManager instance;
    public int randomNbr1;
    public int randomNbr2;

    float timer;

    private void Awake()
    {
        if (instance)
        { Destroy(this); }
        else
        { instance = this; }
    }
     
    void Start()
    {
        RandomNumber();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 30)
        {
            timer = 0;
            RandomNumber();
        }
    }

    public void RandomNumber()
    {
        randomNbr1 = Random.Range(0, listObData.Count);
        randomNbr2 = Random.Range(0, listObData.Count);
        while (randomNbr1 == randomNbr2)
        {
            randomNbr2 = Random.Range(0, listObData.Count);
        }
    }

    
}
