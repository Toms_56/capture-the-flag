using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCreation : MonoBehaviour
{
    public PlayerData player;

    public float healthPts = 3;

    public GameObject prefab;
    //public Vector3 velocity;
    public float speed = 0;
    //public float gravityValue = 1.0f;
    public float jumpHeight = -9.81f ;
    
    public KeyCode moveForward;
    public KeyCode moveBackWard;
    public KeyCode turnLeft;
    public KeyCode turnRight;
    public KeyCode jump;
    public KeyCode switchObject;
    public KeyCode inventoryScroll;
    public KeyCode useObject;

    public virtual void Creation(PlayerData playerData)
    {
        prefab = playerData.prefab;
        healthPts = playerData.healthPts;
        speed = playerData.speed;
        jumpHeight = playerData.jumpHeight;
        moveForward = playerData.moveForward;
        moveBackWard = playerData.moveBackWard;
        turnLeft = playerData.turnLeft;
        turnRight = playerData.turnRight;
        jump = playerData.jump;
        switchObject = playerData.switchObject;
        inventoryScroll = playerData.inventoryScroll;
        useObject = playerData.useObject;
    }
}
