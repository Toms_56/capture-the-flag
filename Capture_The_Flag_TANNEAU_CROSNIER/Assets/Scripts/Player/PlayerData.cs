using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player", menuName = "Create Player")]
public class PlayerData : ScriptableObject
{
    public GameObject prefab;
    public float healthPts = 3;
    
    //public Vector3 velocity;
    public float speed = 0;
    //public float gravityValue = 1.0f;
    public float jumpHeight = -9.81f ;
    
    public KeyCode moveForward;
    public KeyCode moveBackWard;
    public KeyCode turnLeft;
    public KeyCode turnRight;
    public KeyCode jump;
    public KeyCode switchObject;
    public KeyCode inventoryScroll;
    public KeyCode useObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
