using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public Animator anim;
    [SerializeField]private bool isGrounded = false;    
    public PlayerData playerData;
    public float pv;
    public float speed;

    //public GameObject targetPoint;

    //public GameObject playerBag, flag;
    public Rigidbody rb;

    public float rotateSpeed;
    public GameObject spawner;

    // For system inventory
    public Inventory inventory;
    public int index = 0;
    public Text textSwap;
    public bool canSwap = false;
    public string colorTeam;
    public string colorEnemy;

    public bool indestructible = false;
    public GameObject[] seeObject;


    // Start is called before the first frame update
    void Start()
    {
        speed = playerData.speed;
        pv = playerData.healthPts;
        anim = gameObject.GetComponent<Animator>();
        playerData.speed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("Speed", playerData.speed);
        anim.SetBool("isGrounded", isGrounded);
        Move();

        if (Input.GetKey(playerData.turnLeft) || Input.GetKey(playerData.turnRight))
        {
            Rotate(playerData.turnLeft, playerData.turnRight);
        }

        Jump(playerData.jump);

        if (pv <= 0)
        {

            inventory.slots[0].currentItem = null;
            inventory.slots[1].currentItem = null;
            spawner.GetComponent<PlayerSpawner>().Death();
            Destroy(gameObject);
        }

        // For inventory  : Use Item

        if (Input.GetKeyDown(playerData.useObject) && inventory.slots[index].currentItem)
        {
            inventory.slots[index].currentItem.Use();
        }

        // For inventory : Switch betwen item
        if (Input.GetKeyDown(playerData.inventoryScroll))
        {
            index++;

            if (index > inventory.slots.Length - 1)
            {
                index = 0;
            }

            for (int i = 0; i < inventory.slots.Length; i++)
            {
                inventory.slots[i].isEquip = false;
            }
        }

        inventory.slots[index].isEquip = true;

        TxtSwap();
    }

    private void Move()
    {
        if (Input.GetKey(playerData.moveForward))
        {
            playerData.speed = 3;
            transform.Translate (Vector3.forward * Time.deltaTime * playerData.speed);
        }else
        {
            playerData.speed = 0;
        }
        
        if (Input.GetKey(playerData.moveBackWard))
        {
            playerData.speed = 3;
            transform.Translate (-Vector3.forward * Time.deltaTime * playerData.speed);
        }
        
    }

    private void Rotate(KeyCode rl, KeyCode rr)
    {
        if (Input.GetKey(rl))
        {
            transform.Rotate(Vector3.up * -rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey(rr))
        {
            transform.Rotate(Vector3.up * rotateSpeed * Time.deltaTime);
        }
    }

    void Jump(KeyCode jump)
    {
        if (Input.GetKeyDown(jump) && isGrounded)
        {
            if (isGrounded)
            {
                rb.AddForce(Vector3.up * playerData.jumpHeight, ForceMode.Impulse);
                isGrounded = false;
            }
        }
    }
    
    // public void Death()
    // {
    //     Debug.Log("Player " + playerData.name + " is dead");
    //     StartCoroutine(CallSpawner());
    // }

    // IEnumerator CallSpawner()
    // {
    //     yield return new WaitForSeconds(3);
    //     spawner.GetComponent<PlayerSpawner>().SpawnPlayer();
    // }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ground"))
        {
            isGrounded = true;
        }

        // Decrease HP or Destruct Chestplate if it is active
        if (other.tag == "Bullet" ||other.tag == "KnifeBlade")
        {
            if (indestructible)
            {
                for (int i = 0; i < inventory.slots.Length; i++)
                {
                    if (inventory.slots[i].currentItem.GetComponent<Chestplate>())
                    {
                       inventory.slots[i].currentItem.Destruct();
                       inventory.slots[i].icon.color = Color.white;
                    }
                }
            }
            else
            {
                pv--;
            }
        }

        // For inventory 
            // System for add flag in inventory

        if (other.tag == "Flag")
        {
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if (!inventory.slots[i].currentItem)
                {
                    inventory.slots[i].AddObject(other.GetComponent<ObjectCreation>());
                    other.tag = "Untagged";
                    inventory.slots[i].flagEquip = true;
                    other.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    break;
                }
            }
        }

            // System for add items in inventory / 
        Spawner spawner = other.GetComponent<Spawner>();
        if (spawner == null) return;


        if (spawner && other.tag != "Food" && other.tag != "Untagged")
        {
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if (!inventory.slots[i].currentItem)
                {
                    inventory.slots[i].AddObject(other.GetComponentInChildren<ObjectCreation>());
                    SpawnerManager.instance.listObData.Remove(spawner.createObject.dataForList);
                    other.tag = "Untagged";
                    break;
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        Spawner spawner = other.GetComponent<Spawner>();
        // System for swap object
        if (inventory.slots[index].currentItem && canSwap)
        {
            if (Input.GetKey(playerData.switchObject))
            {
                // Check if the object is not a flag
                if (!inventory.slots[index].flagEquip)
                {
                    if (other.tag == "Flag")
                    {
                        // Replace the object in the list 
                        SpawnerManager.instance.listObData.Add(inventory.slots[index].currentItem.dataForList);
                        //Destroy the old object
                        Destroy(inventory.slots[index].currentItem.gameObject);
                        // Add the new object
                        inventory.slots[index].AddObject(other.GetComponent<ObjectCreation>());
                        // Change the tag for no more interaction
                        other.tag = "Untagged";
                        // Make the object invisible
                        other.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    }
                    else if (other.tag == "Zone" + colorEnemy)
                    {
                        ZoneFlag zoneFlag = other.GetComponent<ZoneFlag>();
                        SpawnerManager.instance.listObData.Add(inventory.slots[index].currentItem.dataForList);
                        Destroy(inventory.slots[index].currentItem.gameObject);
                        inventory.slots[index].AddObject(zoneFlag.flag);
                        inventory.slots[index].flagEquip = true;
                        zoneFlag.flagMesh.enabled = false;
                    }
                    else if (spawner && other.tag != "Untagged")
                    {
                        inventory.slots[index].currentItem.SwappingItem(spawner);
                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        canSwap = false;
    }

    // Activate / desactivate the SwapMessage
    private void TxtSwap()
    {
        // Features for indicate the player can swap object
        if (canSwap)
        {
            textSwap.enabled = true;
            textSwap.text = "Press " + playerData.switchObject + " for swap item";
        }
        else
        {
            textSwap.enabled = false;
        }
    }
}
