using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    private GameObject playerPre;
    public PlayerData playerData;
    
 
    // Start is called before the first frame update
    void Start()
    {
        SpawnPlayer();
        
    }

    private void SpawnPlayer()
    {
        playerPre = Instantiate(playerData.prefab, transform);
        playerPre.GetComponent<PlayerCreation>().Creation(playerData);
        playerPre.GetComponent<Player>().spawner = gameObject;
    }
    
    public void Death()
    {
        Debug.Log("Player " + playerData.name + " is dead");
        //Destroy(GetComponentInChildren<Player>().playerData.prefab);
        StartCoroutine(CallSpawner());
    }
    
    public IEnumerator CallSpawner()
    {
        yield return new WaitForSeconds(3);
        SpawnPlayer();
    }
}
