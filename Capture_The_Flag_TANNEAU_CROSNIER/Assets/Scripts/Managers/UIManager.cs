using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject panelPause;
    public bool isPaused = false;
    public Slider healthP1;
    public Slider healthP2;

    public List<PlayerSpawner> spawners;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        isPaused = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPaused)
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                SceneManager.LoadScene(0);
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                Time.timeScale = 0;
                isPaused = true;
                panelPause.SetActive(true);
                
            }else if (isPaused)
            {
                Time.timeScale = 1;
                isPaused = false;
                panelPause.SetActive(false);
            }
        }
        healthP1.value = spawners[0].GetComponentInChildren<Player>().pv;
        healthP2.value = spawners[1].GetComponentInChildren<Player>().pv;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Hit();
        }
    }

    public void Resume()
    {
        isPaused = false;
        Time.timeScale = 1;
        panelPause.SetActive(false);
    }

    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void LeaveGame()
    {
        Application.Quit();
    }
    
    public void Hit()
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            spawners[i].GetComponentInChildren<Player>().pv--;
        }
    }
}
